// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNITY_UnityBlueprintFunctionLibrary_generated_h
#error "UnityBlueprintFunctionLibrary.generated.h already included, missing '#pragma once' in UnityBlueprintFunctionLibrary.h"
#endif
#define UNITY_UnityBlueprintFunctionLibrary_generated_h

#define Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetCurrentPlacement) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Placement); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UUnityBlueprintFunctionLibrary::SetCurrentPlacement(Z_Param_Placement); \
		P_NATIVE_END; \
	}


#define Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetCurrentPlacement) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Placement); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UUnityBlueprintFunctionLibrary::SetCurrentPlacement(Z_Param_Placement); \
		P_NATIVE_END; \
	}


#define Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUnityBlueprintFunctionLibrary(); \
	friend UNITY_API class UClass* Z_Construct_UClass_UUnityBlueprintFunctionLibrary(); \
public: \
	DECLARE_CLASS(UUnityBlueprintFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Unity"), NO_API) \
	DECLARE_SERIALIZER(UUnityBlueprintFunctionLibrary) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUUnityBlueprintFunctionLibrary(); \
	friend UNITY_API class UClass* Z_Construct_UClass_UUnityBlueprintFunctionLibrary(); \
public: \
	DECLARE_CLASS(UUnityBlueprintFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Unity"), NO_API) \
	DECLARE_SERIALIZER(UUnityBlueprintFunctionLibrary) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUnityBlueprintFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUnityBlueprintFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUnityBlueprintFunctionLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUnityBlueprintFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUnityBlueprintFunctionLibrary(UUnityBlueprintFunctionLibrary&&); \
	NO_API UUnityBlueprintFunctionLibrary(const UUnityBlueprintFunctionLibrary&); \
public:


#define Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUnityBlueprintFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUnityBlueprintFunctionLibrary(UUnityBlueprintFunctionLibrary&&); \
	NO_API UUnityBlueprintFunctionLibrary(const UUnityBlueprintFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUnityBlueprintFunctionLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUnityBlueprintFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUnityBlueprintFunctionLibrary)


#define Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_PRIVATE_PROPERTY_OFFSET
#define Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_12_PROLOG
#define Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_RPC_WRAPPERS \
	Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_INCLASS \
	Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_INCLASS_NO_PURE_DECLS \
	Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Bio2_Plugins_AdCollection_Source_Unity_Public_UnityBlueprintFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
