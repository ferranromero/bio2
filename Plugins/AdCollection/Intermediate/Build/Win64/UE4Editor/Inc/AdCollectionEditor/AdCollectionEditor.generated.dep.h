// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#ifndef ADCOLLECTIONEDITOR_AdMobSetting_generated_h
	#include "Private/AdMobSetting.h"
#endif
#ifndef ADCOLLECTIONEDITOR_ChartBoostSetting_generated_h
	#include "Private/ChartBoostSetting.h"
#endif
#ifndef ADCOLLECTIONEDITOR_FacebookSetting_generated_h
	#include "Private/FacebookSetting.h"
#endif
#ifndef ADCOLLECTIONEDITOR_UnitySetting_generated_h
	#include "Private/UnitySetting.h"
#endif
