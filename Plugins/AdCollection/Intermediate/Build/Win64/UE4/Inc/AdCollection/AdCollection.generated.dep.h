// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#ifndef ADCOLLECTION_AdCollectionBPLibrary_generated_h
	#include "Public/AdCollectionBPLibrary.h"
#endif
#ifndef ADCOLLECTION_PlayRewardVideoCallbackProxy_generated_h
	#include "Public/PlayRewardVideoCallbackProxy.h"
#endif
#ifndef ADCOLLECTION_ShowInterstitialCallbackProxy_generated_h
	#include "Public/ShowInterstitialCallbackProxy.h"
#endif
