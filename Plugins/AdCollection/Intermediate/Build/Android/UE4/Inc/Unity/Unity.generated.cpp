// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/Unity.h"
#include "Unity.generated.dep.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode1Unity() {}
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	ENGINE_API class UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();

	UNITY_API class UFunction* Z_Construct_UFunction_UUnityBlueprintFunctionLibrary_SetCurrentPlacement();
	UNITY_API class UClass* Z_Construct_UClass_UUnityBlueprintFunctionLibrary_NoRegister();
	UNITY_API class UClass* Z_Construct_UClass_UUnityBlueprintFunctionLibrary();
	UNITY_API class UPackage* Z_Construct_UPackage__Script_Unity();
	void UUnityBlueprintFunctionLibrary::StaticRegisterNativesUUnityBlueprintFunctionLibrary()
	{
		UClass* Class = UUnityBlueprintFunctionLibrary::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "SetCurrentPlacement", (Native)&UUnityBlueprintFunctionLibrary::execSetCurrentPlacement },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 1);
	}
	UFunction* Z_Construct_UFunction_UUnityBlueprintFunctionLibrary_SetCurrentPlacement()
	{
		struct UnityBlueprintFunctionLibrary_eventSetCurrentPlacement_Parms
		{
			FString Placement;
		};
		UObject* Outer=Z_Construct_UClass_UUnityBlueprintFunctionLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SetCurrentPlacement"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535, sizeof(UnityBlueprintFunctionLibrary_eventSetCurrentPlacement_Parms));
			UProperty* NewProp_Placement = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Placement"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(Placement, UnityBlueprintFunctionLibrary_eventSetCurrentPlacement_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Unity"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("SetCurrentPlacement"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("Placement"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/UnityBlueprintFunctionLibrary.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("set current placement\n@param        placement"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UUnityBlueprintFunctionLibrary_NoRegister()
	{
		return UUnityBlueprintFunctionLibrary::StaticClass();
	}
	UClass* Z_Construct_UClass_UUnityBlueprintFunctionLibrary()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UBlueprintFunctionLibrary();
			Z_Construct_UPackage__Script_Unity();
			OuterClass = UUnityBlueprintFunctionLibrary::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20100080;

				OuterClass->LinkChild(Z_Construct_UFunction_UUnityBlueprintFunctionLibrary_SetCurrentPlacement());

				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UUnityBlueprintFunctionLibrary_SetCurrentPlacement(), "SetCurrentPlacement"); // 3768080881
				static TCppClassTypeInfo<TCppClassTypeTraits<UUnityBlueprintFunctionLibrary> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("UnityBlueprintFunctionLibrary.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/UnityBlueprintFunctionLibrary.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUnityBlueprintFunctionLibrary, 2029742426);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUnityBlueprintFunctionLibrary(Z_Construct_UClass_UUnityBlueprintFunctionLibrary, &UUnityBlueprintFunctionLibrary::StaticClass, TEXT("/Script/Unity"), TEXT("UUnityBlueprintFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUnityBlueprintFunctionLibrary);
	UPackage* Z_Construct_UPackage__Script_Unity()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), nullptr, FName(TEXT("/Script/Unity")), false, false));
			ReturnPackage->SetPackageFlags(PKG_CompiledIn | 0x00000000);
			FGuid Guid;
			Guid.A = 0xC0A9AE5C;
			Guid.B = 0x745C3C76;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
