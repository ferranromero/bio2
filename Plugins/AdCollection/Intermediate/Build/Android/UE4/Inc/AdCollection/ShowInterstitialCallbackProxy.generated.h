// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EAdType : uint8;
class UShowInterstitialCallbackProxy;
#ifdef ADCOLLECTION_ShowInterstitialCallbackProxy_generated_h
#error "ShowInterstitialCallbackProxy.generated.h already included, missing '#pragma once' in ShowInterstitialCallbackProxy.h"
#endif
#define ADCOLLECTION_ShowInterstitialCallbackProxy_generated_h

#define Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execShowInterstitial) \
	{ \
		P_GET_ENUM(EAdType,Z_Param_AdType); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UShowInterstitialCallbackProxy**)Z_Param__Result=UShowInterstitialCallbackProxy::ShowInterstitial(EAdType(Z_Param_AdType)); \
		P_NATIVE_END; \
	}


#define Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execShowInterstitial) \
	{ \
		P_GET_ENUM(EAdType,Z_Param_AdType); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UShowInterstitialCallbackProxy**)Z_Param__Result=UShowInterstitialCallbackProxy::ShowInterstitial(EAdType(Z_Param_AdType)); \
		P_NATIVE_END; \
	}


#define Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShowInterstitialCallbackProxy(); \
	friend ADCOLLECTION_API class UClass* Z_Construct_UClass_UShowInterstitialCallbackProxy(); \
public: \
	DECLARE_CLASS(UShowInterstitialCallbackProxy, UBlueprintAsyncActionBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/AdCollection"), NO_API) \
	DECLARE_SERIALIZER(UShowInterstitialCallbackProxy) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUShowInterstitialCallbackProxy(); \
	friend ADCOLLECTION_API class UClass* Z_Construct_UClass_UShowInterstitialCallbackProxy(); \
public: \
	DECLARE_CLASS(UShowInterstitialCallbackProxy, UBlueprintAsyncActionBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/AdCollection"), NO_API) \
	DECLARE_SERIALIZER(UShowInterstitialCallbackProxy) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShowInterstitialCallbackProxy(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShowInterstitialCallbackProxy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShowInterstitialCallbackProxy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShowInterstitialCallbackProxy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShowInterstitialCallbackProxy(UShowInterstitialCallbackProxy&&); \
	NO_API UShowInterstitialCallbackProxy(const UShowInterstitialCallbackProxy&); \
public:


#define Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShowInterstitialCallbackProxy(UShowInterstitialCallbackProxy&&); \
	NO_API UShowInterstitialCallbackProxy(const UShowInterstitialCallbackProxy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShowInterstitialCallbackProxy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShowInterstitialCallbackProxy); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UShowInterstitialCallbackProxy)


#define Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_PRIVATE_PROPERTY_OFFSET
#define Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_14_PROLOG
#define Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_PRIVATE_PROPERTY_OFFSET \
	Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_RPC_WRAPPERS \
	Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_INCLASS \
	Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_PRIVATE_PROPERTY_OFFSET \
	Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_INCLASS_NO_PURE_DECLS \
	Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Bio2_Plugins_AdCollection_Source_AdCollection_Public_ShowInterstitialCallbackProxy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
