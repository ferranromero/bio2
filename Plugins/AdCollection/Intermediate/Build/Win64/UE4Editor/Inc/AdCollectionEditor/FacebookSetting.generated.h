// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ADCOLLECTIONEDITOR_FacebookSetting_generated_h
#error "FacebookSetting.generated.h already included, missing '#pragma once' in FacebookSetting.h"
#endif
#define ADCOLLECTIONEDITOR_FacebookSetting_generated_h

#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_RPC_WRAPPERS
#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFacebookSetting(); \
	friend ADCOLLECTIONEDITOR_API class UClass* Z_Construct_UClass_UFacebookSetting(); \
public: \
	DECLARE_CLASS(UFacebookSetting, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/AdCollectionEditor"), NO_API) \
	DECLARE_SERIALIZER(UFacebookSetting) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUFacebookSetting(); \
	friend ADCOLLECTIONEDITOR_API class UClass* Z_Construct_UClass_UFacebookSetting(); \
public: \
	DECLARE_CLASS(UFacebookSetting, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/AdCollectionEditor"), NO_API) \
	DECLARE_SERIALIZER(UFacebookSetting) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFacebookSetting(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFacebookSetting) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFacebookSetting); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFacebookSetting); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFacebookSetting(UFacebookSetting&&); \
	NO_API UFacebookSetting(const UFacebookSetting&); \
public:


#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFacebookSetting(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFacebookSetting(UFacebookSetting&&); \
	NO_API UFacebookSetting(const UFacebookSetting&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFacebookSetting); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFacebookSetting); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFacebookSetting)


#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bAndroidEnabled() { return STRUCT_OFFSET(UFacebookSetting, bAndroidEnabled); } \
	FORCEINLINE static uint32 __PPO__AndroidBannerUnit() { return STRUCT_OFFSET(UFacebookSetting, AndroidBannerUnit); } \
	FORCEINLINE static uint32 __PPO__AndroidInterstitialUnit() { return STRUCT_OFFSET(UFacebookSetting, AndroidInterstitialUnit); } \
	FORCEINLINE static uint32 __PPO__AndroidRewardedVideoAdUnit() { return STRUCT_OFFSET(UFacebookSetting, AndroidRewardedVideoAdUnit); } \
	FORCEINLINE static uint32 __PPO__bIOSEnabled() { return STRUCT_OFFSET(UFacebookSetting, bIOSEnabled); } \
	FORCEINLINE static uint32 __PPO__IOSBannerUnit() { return STRUCT_OFFSET(UFacebookSetting, IOSBannerUnit); } \
	FORCEINLINE static uint32 __PPO__IOSInterstitialUnit() { return STRUCT_OFFSET(UFacebookSetting, IOSInterstitialUnit); } \
	FORCEINLINE static uint32 __PPO__IOSRewardedVideoAdUnit() { return STRUCT_OFFSET(UFacebookSetting, IOSRewardedVideoAdUnit); }


#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_15_PROLOG
#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_PRIVATE_PROPERTY_OFFSET \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_RPC_WRAPPERS \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_INCLASS \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_PRIVATE_PROPERTY_OFFSET \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_INCLASS_NO_PURE_DECLS \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_FacebookSetting_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
