// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/AdCollection.h"
#include "AdCollection.generated.dep.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode1AdCollection() {}
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	ENGINE_API class UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	ENGINE_API class UClass* Z_Construct_UClass_UBlueprintAsyncActionBase();

	ADCOLLECTION_API class UFunction* Z_Construct_UDelegateFunction_AdCollection_DynamicInterstitialCloseDelegate__DelegateSignature();
	ADCOLLECTION_API class UFunction* Z_Construct_UDelegateFunction_AdCollection_DynamicInterstitialClickDelegate__DelegateSignature();
	ADCOLLECTION_API class UFunction* Z_Construct_UDelegateFunction_AdCollection_DynamicInterstitialShowDelegate__DelegateSignature();
	ADCOLLECTION_API class UFunction* Z_Construct_UDelegateFunction_AdCollection_DynamicRewardedClosedDelegate__DelegateSignature();
	ADCOLLECTION_API class UFunction* Z_Construct_UDelegateFunction_AdCollection_PlayRewardedDelegate__DelegateSignature();
	ADCOLLECTION_API class UEnum* Z_Construct_UEnum_AdCollection_EAdType();
	ADCOLLECTION_API class UScriptStruct* Z_Construct_UScriptStruct_FRewardedStatus();
	ADCOLLECTION_API class UScriptStruct* Z_Construct_UScriptStruct_FAdMobRewardItem();
	ADCOLLECTION_API class UFunction* Z_Construct_UFunction_UAdCollectionBPLibrary_HideBanner();
	ADCOLLECTION_API class UFunction* Z_Construct_UFunction_UAdCollectionBPLibrary_IsBannerReady();
	ADCOLLECTION_API class UFunction* Z_Construct_UFunction_UAdCollectionBPLibrary_IsInterstitialReady();
	ADCOLLECTION_API class UFunction* Z_Construct_UFunction_UAdCollectionBPLibrary_IsRewardedVideoReady();
	ADCOLLECTION_API class UFunction* Z_Construct_UFunction_UAdCollectionBPLibrary_PlayAdVideo();
	ADCOLLECTION_API class UFunction* Z_Construct_UFunction_UAdCollectionBPLibrary_ShowBanner();
	ADCOLLECTION_API class UClass* Z_Construct_UClass_UAdCollectionBPLibrary_NoRegister();
	ADCOLLECTION_API class UClass* Z_Construct_UClass_UAdCollectionBPLibrary();
	ADCOLLECTION_API class UFunction* Z_Construct_UFunction_UPlayRewardVideoCallbackProxy_PlayRewardedVideo();
	ADCOLLECTION_API class UClass* Z_Construct_UClass_UPlayRewardVideoCallbackProxy_NoRegister();
	ADCOLLECTION_API class UClass* Z_Construct_UClass_UPlayRewardVideoCallbackProxy();
	ADCOLLECTION_API class UFunction* Z_Construct_UFunction_UShowInterstitialCallbackProxy_ShowInterstitial();
	ADCOLLECTION_API class UClass* Z_Construct_UClass_UShowInterstitialCallbackProxy_NoRegister();
	ADCOLLECTION_API class UClass* Z_Construct_UClass_UShowInterstitialCallbackProxy();
	ADCOLLECTION_API class UPackage* Z_Construct_UPackage__Script_AdCollection();
	UFunction* Z_Construct_UDelegateFunction_AdCollection_DynamicInterstitialCloseDelegate__DelegateSignature()
	{
		UObject* Outer=Z_Construct_UPackage__Script_AdCollection();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("DynamicInterstitialCloseDelegate__DelegateSignature"), RF_Public|RF_Transient|RF_MarkAsNative) UDelegateFunction(FObjectInitializer(), NULL, 0x00130000, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UDelegateFunction_AdCollection_DynamicInterstitialClickDelegate__DelegateSignature()
	{
		UObject* Outer=Z_Construct_UPackage__Script_AdCollection();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("DynamicInterstitialClickDelegate__DelegateSignature"), RF_Public|RF_Transient|RF_MarkAsNative) UDelegateFunction(FObjectInitializer(), NULL, 0x00130000, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UDelegateFunction_AdCollection_DynamicInterstitialShowDelegate__DelegateSignature()
	{
		UObject* Outer=Z_Construct_UPackage__Script_AdCollection();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("DynamicInterstitialShowDelegate__DelegateSignature"), RF_Public|RF_Transient|RF_MarkAsNative) UDelegateFunction(FObjectInitializer(), NULL, 0x00130000, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UDelegateFunction_AdCollection_DynamicRewardedClosedDelegate__DelegateSignature()
	{
		UObject* Outer=Z_Construct_UPackage__Script_AdCollection();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("DynamicRewardedClosedDelegate__DelegateSignature"), RF_Public|RF_Transient|RF_MarkAsNative) UDelegateFunction(FObjectInitializer(), NULL, 0x00130000, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UDelegateFunction_AdCollection_PlayRewardedDelegate__DelegateSignature()
	{
		struct _Script_AdCollection_eventPlayRewardedDelegate_Parms
		{
			FRewardedStatus RewardStatus;
		};
		UObject* Outer=Z_Construct_UPackage__Script_AdCollection();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("PlayRewardedDelegate__DelegateSignature"), RF_Public|RF_Transient|RF_MarkAsNative) UDelegateFunction(FObjectInitializer(), NULL, 0x00130000, 65535, sizeof(_Script_AdCollection_eventPlayRewardedDelegate_Parms));
			UProperty* NewProp_RewardStatus = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("RewardStatus"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(RewardStatus, _Script_AdCollection_eventPlayRewardedDelegate_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FRewardedStatus());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
#endif
		}
		return ReturnFunction;
	}
static UEnum* EAdType_StaticEnum()
{
	extern ADCOLLECTION_API class UPackage* Z_Construct_UPackage__Script_AdCollection();
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		extern ADCOLLECTION_API class UEnum* Z_Construct_UEnum_AdCollection_EAdType();
		Singleton = GetStaticEnum(Z_Construct_UEnum_AdCollection_EAdType, Z_Construct_UPackage__Script_AdCollection(), TEXT("EAdType"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAdType(EAdType_StaticEnum, TEXT("/Script/AdCollection"), TEXT("EAdType"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_AdCollection_EAdType()
	{
		UPackage* Outer=Z_Construct_UPackage__Script_AdCollection();
		extern uint32 Get_Z_Construct_UEnum_AdCollection_EAdType_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAdType"), 0, Get_Z_Construct_UEnum_AdCollection_EAdType_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EAdType"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("EAdType::AdMob"), 0);
			EnumNames.Emplace(TEXT("EAdType::Unity"), 1);
			EnumNames.Emplace(TEXT("EAdType::ChartBoost"), 2);
			EnumNames.Emplace(TEXT("EAdType::FacebookAds"), 3);
			EnumNames.Emplace(TEXT("EAdType::EAdType_MAX"), 4);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("EAdType");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("AdMob.DisplayName"), TEXT("AdMob"));
			MetaData->SetValue(ReturnEnum, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnEnum, TEXT("ChartBoost.DisplayName"), TEXT("ChartBoost"));
			MetaData->SetValue(ReturnEnum, TEXT("ChartBoost.ToolTip"), TEXT("the unity ads type"));
			MetaData->SetValue(ReturnEnum, TEXT("FacebookAds.DisplayName"), TEXT("Facebook"));
			MetaData->SetValue(ReturnEnum, TEXT("FacebookAds.ToolTip"), TEXT("the chartboost ads type"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(ReturnEnum, TEXT("Unity.DisplayName"), TEXT("Unity"));
			MetaData->SetValue(ReturnEnum, TEXT("Unity.ToolTip"), TEXT("the admob ads type"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_AdCollection_EAdType_CRC() { return 2305985711U; }
class UScriptStruct* FRewardedStatus::StaticStruct()
{
	extern ADCOLLECTION_API class UPackage* Z_Construct_UPackage__Script_AdCollection();
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ADCOLLECTION_API class UScriptStruct* Z_Construct_UScriptStruct_FRewardedStatus();
		extern ADCOLLECTION_API uint32 Get_Z_Construct_UScriptStruct_FRewardedStatus_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRewardedStatus, Z_Construct_UPackage__Script_AdCollection(), TEXT("RewardedStatus"), sizeof(FRewardedStatus), Get_Z_Construct_UScriptStruct_FRewardedStatus_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRewardedStatus(FRewardedStatus::StaticStruct, TEXT("/Script/AdCollection"), TEXT("RewardedStatus"), false, nullptr, nullptr);
static struct FScriptStruct_AdCollection_StaticRegisterNativesFRewardedStatus
{
	FScriptStruct_AdCollection_StaticRegisterNativesFRewardedStatus()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("RewardedStatus")),new UScriptStruct::TCppStructOps<FRewardedStatus>);
	}
} ScriptStruct_AdCollection_StaticRegisterNativesFRewardedStatus;
	UScriptStruct* Z_Construct_UScriptStruct_FRewardedStatus()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_AdCollection();
		extern uint32 Get_Z_Construct_UScriptStruct_FRewardedStatus_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RewardedStatus"), sizeof(FRewardedStatus), Get_Z_Construct_UScriptStruct_FRewardedStatus_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("RewardedStatus"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FRewardedStatus>, EStructFlags(0x00000001));
			UProperty* NewProp_ChartBoostReward = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("ChartBoostReward"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ChartBoostReward, FRewardedStatus), 0x0010000000000015);
			UProperty* NewProp_AdMobItem = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("AdMobItem"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(AdMobItem, FRewardedStatus), 0x0010000000000015, Z_Construct_UScriptStruct_FAdMobRewardItem());
			UProperty* NewProp_AdType = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("AdType"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(AdType, FRewardedStatus), 0x0010000000000015, Z_Construct_UEnum_AdCollection_EAdType());
			UProperty* NewProp_AdType_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_AdType, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(NewProp_ChartBoostReward, TEXT("Category"), TEXT("RewardedStatus"));
			MetaData->SetValue(NewProp_ChartBoostReward, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(NewProp_ChartBoostReward, TEXT("ToolTip"), TEXT("if the AdType is ChartBoost the value is the reward value"));
			MetaData->SetValue(NewProp_AdMobItem, TEXT("Category"), TEXT("RewardedStatus"));
			MetaData->SetValue(NewProp_AdMobItem, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(NewProp_AdMobItem, TEXT("ToolTip"), TEXT("if the AdType is AdMob, the AdmobItem is valid"));
			MetaData->SetValue(NewProp_AdType, TEXT("Category"), TEXT("RewardedStatus"));
			MetaData->SetValue(NewProp_AdType, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(NewProp_AdType, TEXT("ToolTip"), TEXT("the rewarded ads type"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRewardedStatus_CRC() { return 163513484U; }
class UScriptStruct* FAdMobRewardItem::StaticStruct()
{
	extern ADCOLLECTION_API class UPackage* Z_Construct_UPackage__Script_AdCollection();
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ADCOLLECTION_API class UScriptStruct* Z_Construct_UScriptStruct_FAdMobRewardItem();
		extern ADCOLLECTION_API uint32 Get_Z_Construct_UScriptStruct_FAdMobRewardItem_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAdMobRewardItem, Z_Construct_UPackage__Script_AdCollection(), TEXT("AdMobRewardItem"), sizeof(FAdMobRewardItem), Get_Z_Construct_UScriptStruct_FAdMobRewardItem_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAdMobRewardItem(FAdMobRewardItem::StaticStruct, TEXT("/Script/AdCollection"), TEXT("AdMobRewardItem"), false, nullptr, nullptr);
static struct FScriptStruct_AdCollection_StaticRegisterNativesFAdMobRewardItem
{
	FScriptStruct_AdCollection_StaticRegisterNativesFAdMobRewardItem()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("AdMobRewardItem")),new UScriptStruct::TCppStructOps<FAdMobRewardItem>);
	}
} ScriptStruct_AdCollection_StaticRegisterNativesFAdMobRewardItem;
	UScriptStruct* Z_Construct_UScriptStruct_FAdMobRewardItem()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_AdCollection();
		extern uint32 Get_Z_Construct_UScriptStruct_FAdMobRewardItem_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AdMobRewardItem"), sizeof(FAdMobRewardItem), Get_Z_Construct_UScriptStruct_FAdMobRewardItem_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("AdMobRewardItem"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FAdMobRewardItem>, EStructFlags(0x00000001));
			UProperty* NewProp_Amount = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("Amount"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(Amount, FAdMobRewardItem), 0x0010000000000015);
			UProperty* NewProp_Type = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("Type"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(Type, FAdMobRewardItem), 0x0010000000000015);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(NewProp_Amount, TEXT("Category"), TEXT("AdMobRewardItem"));
			MetaData->SetValue(NewProp_Amount, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(NewProp_Amount, TEXT("ToolTip"), TEXT("the admob rewarded item value"));
			MetaData->SetValue(NewProp_Type, TEXT("Category"), TEXT("AdMobRewardItem"));
			MetaData->SetValue(NewProp_Type, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(NewProp_Type, TEXT("ToolTip"), TEXT("the admob rewarded item type"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAdMobRewardItem_CRC() { return 3610565261U; }
	void UAdCollectionBPLibrary::StaticRegisterNativesUAdCollectionBPLibrary()
	{
		UClass* Class = UAdCollectionBPLibrary::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "HideBanner", (Native)&UAdCollectionBPLibrary::execHideBanner },
			{ "IsBannerReady", (Native)&UAdCollectionBPLibrary::execIsBannerReady },
			{ "IsInterstitialReady", (Native)&UAdCollectionBPLibrary::execIsInterstitialReady },
			{ "IsRewardedVideoReady", (Native)&UAdCollectionBPLibrary::execIsRewardedVideoReady },
			{ "PlayAdVideo", (Native)&UAdCollectionBPLibrary::execPlayAdVideo },
			{ "ShowBanner", (Native)&UAdCollectionBPLibrary::execShowBanner },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 6);
	}
	UFunction* Z_Construct_UFunction_UAdCollectionBPLibrary_HideBanner()
	{
		struct AdCollectionBPLibrary_eventHideBanner_Parms
		{
			EAdType adType;
		};
		UObject* Outer=Z_Construct_UClass_UAdCollectionBPLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("HideBanner"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535, sizeof(AdCollectionBPLibrary_eventHideBanner_Parms));
			UProperty* NewProp_adType = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("adType"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(adType, AdCollectionBPLibrary_eventHideBanner_Parms), 0x0010000000000080, Z_Construct_UEnum_AdCollection_EAdType());
			UProperty* NewProp_adType_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_adType, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AdCollection"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("HideBanner"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("AdCollection Hide Banner"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("hide  banner\n@param        adType                  the  ads type"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UAdCollectionBPLibrary_IsBannerReady()
	{
		struct AdCollectionBPLibrary_eventIsBannerReady_Parms
		{
			EAdType adType;
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UAdCollectionBPLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("IsBannerReady"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x14022401, 65535, sizeof(AdCollectionBPLibrary_eventIsBannerReady_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, AdCollectionBPLibrary_eventIsBannerReady_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, AdCollectionBPLibrary_eventIsBannerReady_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, AdCollectionBPLibrary_eventIsBannerReady_Parms), sizeof(bool), true);
			UProperty* NewProp_adType = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("adType"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(adType, AdCollectionBPLibrary_eventIsBannerReady_Parms), 0x0010000000000080, Z_Construct_UEnum_AdCollection_EAdType());
			UProperty* NewProp_adType_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_adType, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AdCollection"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("IsBannerAdsReady"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("AdCollection Check Banner Ads Ready"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("check is the banner is load finish\n@param        adType                  the  ads type"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UAdCollectionBPLibrary_IsInterstitialReady()
	{
		struct AdCollectionBPLibrary_eventIsInterstitialReady_Parms
		{
			EAdType adType;
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UAdCollectionBPLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("IsInterstitialReady"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x14022401, 65535, sizeof(AdCollectionBPLibrary_eventIsInterstitialReady_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, AdCollectionBPLibrary_eventIsInterstitialReady_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, AdCollectionBPLibrary_eventIsInterstitialReady_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, AdCollectionBPLibrary_eventIsInterstitialReady_Parms), sizeof(bool), true);
			UProperty* NewProp_adType = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("adType"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(adType, AdCollectionBPLibrary_eventIsInterstitialReady_Parms), 0x0010000000000080, Z_Construct_UEnum_AdCollection_EAdType());
			UProperty* NewProp_adType_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_adType, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AdCollection"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("IsInterstitialAdsReady"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("AdCollection Check Interstital Ads Ready"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("check is the interstitial is load finish\n@param        adType                  the  ads type"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UAdCollectionBPLibrary_IsRewardedVideoReady()
	{
		struct AdCollectionBPLibrary_eventIsRewardedVideoReady_Parms
		{
			EAdType adType;
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UAdCollectionBPLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("IsRewardedVideoReady"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x14022401, 65535, sizeof(AdCollectionBPLibrary_eventIsRewardedVideoReady_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, AdCollectionBPLibrary_eventIsRewardedVideoReady_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, AdCollectionBPLibrary_eventIsRewardedVideoReady_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, AdCollectionBPLibrary_eventIsRewardedVideoReady_Parms), sizeof(bool), true);
			UProperty* NewProp_adType = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("adType"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(adType, AdCollectionBPLibrary_eventIsRewardedVideoReady_Parms), 0x0010000000000080, Z_Construct_UEnum_AdCollection_EAdType());
			UProperty* NewProp_adType_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_adType, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AdCollection"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("IsRewardedVideoAdsReady"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("AdCollection Check RewardedVideo Ads Ready"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("check is the rewardedvideo ads load finish\n@param        adType                  the  ads type"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UAdCollectionBPLibrary_PlayAdVideo()
	{
		struct AdCollectionBPLibrary_eventPlayAdVideo_Parms
		{
			EAdType adType;
		};
		UObject* Outer=Z_Construct_UClass_UAdCollectionBPLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("PlayAdVideo"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535, sizeof(AdCollectionBPLibrary_eventPlayAdVideo_Parms));
			UProperty* NewProp_adType = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("adType"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(adType, AdCollectionBPLibrary_eventPlayAdVideo_Parms), 0x0010000000000080, Z_Construct_UEnum_AdCollection_EAdType());
			UProperty* NewProp_adType_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_adType, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AdCollection"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("SimplePlayRewardedVideo"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("AdCollection Play"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("play rewardedvideo ads\n@param        adType                  the ads type"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UAdCollectionBPLibrary_ShowBanner()
	{
		struct AdCollectionBPLibrary_eventShowBanner_Parms
		{
			EAdType adType;
			bool isOnBottom;
		};
		UObject* Outer=Z_Construct_UClass_UAdCollectionBPLibrary();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ShowBanner"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535, sizeof(AdCollectionBPLibrary_eventShowBanner_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(isOnBottom, AdCollectionBPLibrary_eventShowBanner_Parms, bool);
			UProperty* NewProp_isOnBottom = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("isOnBottom"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(isOnBottom, AdCollectionBPLibrary_eventShowBanner_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(isOnBottom, AdCollectionBPLibrary_eventShowBanner_Parms), sizeof(bool), true);
			UProperty* NewProp_adType = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("adType"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(adType, AdCollectionBPLibrary_eventShowBanner_Parms), 0x0010000000000080, Z_Construct_UEnum_AdCollection_EAdType());
			UProperty* NewProp_adType_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_adType, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AdCollection"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("ShowBanner"));
			MetaData->SetValue(ReturnFunction, TEXT("Keywords"), TEXT("AdCollection Show Banner"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Show  banner\n@param        isOnBottom              if the banner show on the bottom of the screen"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAdCollectionBPLibrary_NoRegister()
	{
		return UAdCollectionBPLibrary::StaticClass();
	}
	UClass* Z_Construct_UClass_UAdCollectionBPLibrary()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UBlueprintFunctionLibrary();
			Z_Construct_UPackage__Script_AdCollection();
			OuterClass = UAdCollectionBPLibrary::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20000080;

				OuterClass->LinkChild(Z_Construct_UFunction_UAdCollectionBPLibrary_HideBanner());
				OuterClass->LinkChild(Z_Construct_UFunction_UAdCollectionBPLibrary_IsBannerReady());
				OuterClass->LinkChild(Z_Construct_UFunction_UAdCollectionBPLibrary_IsInterstitialReady());
				OuterClass->LinkChild(Z_Construct_UFunction_UAdCollectionBPLibrary_IsRewardedVideoReady());
				OuterClass->LinkChild(Z_Construct_UFunction_UAdCollectionBPLibrary_PlayAdVideo());
				OuterClass->LinkChild(Z_Construct_UFunction_UAdCollectionBPLibrary_ShowBanner());

				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UAdCollectionBPLibrary_HideBanner(), "HideBanner"); // 2076902763
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UAdCollectionBPLibrary_IsBannerReady(), "IsBannerReady"); // 2247133609
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UAdCollectionBPLibrary_IsInterstitialReady(), "IsInterstitialReady"); // 3148426086
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UAdCollectionBPLibrary_IsRewardedVideoReady(), "IsRewardedVideoReady"); // 1295105319
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UAdCollectionBPLibrary_PlayAdVideo(), "PlayAdVideo"); // 1784301706
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UAdCollectionBPLibrary_ShowBanner(), "ShowBanner"); // 848060124
				static TCppClassTypeInfo<TCppClassTypeTraits<UAdCollectionBPLibrary> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("AdCollectionBPLibrary.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/AdCollectionBPLibrary.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAdCollectionBPLibrary, 2399557190);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAdCollectionBPLibrary(Z_Construct_UClass_UAdCollectionBPLibrary, &UAdCollectionBPLibrary::StaticClass, TEXT("/Script/AdCollection"), TEXT("UAdCollectionBPLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAdCollectionBPLibrary);
	void UPlayRewardVideoCallbackProxy::StaticRegisterNativesUPlayRewardVideoCallbackProxy()
	{
		UClass* Class = UPlayRewardVideoCallbackProxy::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "PlayRewardedVideo", (Native)&UPlayRewardVideoCallbackProxy::execPlayRewardedVideo },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 1);
	}
	UFunction* Z_Construct_UFunction_UPlayRewardVideoCallbackProxy_PlayRewardedVideo()
	{
		struct PlayRewardVideoCallbackProxy_eventPlayRewardedVideo_Parms
		{
			EAdType AdType;
			UPlayRewardVideoCallbackProxy* ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UPlayRewardVideoCallbackProxy();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("PlayRewardedVideo"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535, sizeof(PlayRewardVideoCallbackProxy_eventPlayRewardedVideo_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ReturnValue, PlayRewardVideoCallbackProxy_eventPlayRewardedVideo_Parms), 0x0010000000000580, Z_Construct_UClass_UPlayRewardVideoCallbackProxy_NoRegister());
			UProperty* NewProp_AdType = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("AdType"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(AdType, PlayRewardVideoCallbackProxy_eventPlayRewardedVideo_Parms), 0x0010000000000080, Z_Construct_UEnum_AdCollection_EAdType());
			UProperty* NewProp_AdType_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_AdType, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("BlueprintInternalUseOnly"), TEXT("true"));
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AdCollection"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("PlayRewardedVideo"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/PlayRewardVideoCallbackProxy.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("play rewarded video ads\n@param        AdType          the type of the ads"));
			MetaData->SetValue(ReturnFunction, TEXT("WorldContext"), TEXT("WorldContextObject"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPlayRewardVideoCallbackProxy_NoRegister()
	{
		return UPlayRewardVideoCallbackProxy::StaticClass();
	}
	UClass* Z_Construct_UClass_UPlayRewardVideoCallbackProxy()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UBlueprintAsyncActionBase();
			Z_Construct_UPackage__Script_AdCollection();
			OuterClass = UPlayRewardVideoCallbackProxy::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20880080;

				OuterClass->LinkChild(Z_Construct_UFunction_UPlayRewardVideoCallbackProxy_PlayRewardedVideo());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_OnClosed = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OnClosed"), RF_Public|RF_Transient|RF_MarkAsNative) UMulticastDelegateProperty(CPP_PROPERTY_BASE(OnClosed, UPlayRewardVideoCallbackProxy), 0x0010000010080000, Z_Construct_UDelegateFunction_AdCollection_DynamicRewardedClosedDelegate__DelegateSignature());
				UProperty* NewProp_OnSuccess = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OnSuccess"), RF_Public|RF_Transient|RF_MarkAsNative) UMulticastDelegateProperty(CPP_PROPERTY_BASE(OnSuccess, UPlayRewardVideoCallbackProxy), 0x0010000010080000, Z_Construct_UDelegateFunction_AdCollection_PlayRewardedDelegate__DelegateSignature());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UPlayRewardVideoCallbackProxy_PlayRewardedVideo(), "PlayRewardedVideo"); // 1448942894
				static TCppClassTypeInfo<TCppClassTypeTraits<UPlayRewardVideoCallbackProxy> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("PlayRewardVideoCallbackProxy.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/PlayRewardVideoCallbackProxy.h"));
				MetaData->SetValue(OuterClass, TEXT("ToolTip"), TEXT("Async play rewarded video action, when play finished success, the OnSuccess will be called."));
				MetaData->SetValue(NewProp_OnClosed, TEXT("ModuleRelativePath"), TEXT("Public/PlayRewardVideoCallbackProxy.h"));
				MetaData->SetValue(NewProp_OnSuccess, TEXT("ModuleRelativePath"), TEXT("Public/PlayRewardVideoCallbackProxy.h"));
				MetaData->SetValue(NewProp_OnSuccess, TEXT("ToolTip"), TEXT("Called when the Rewarded video ads complete"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlayRewardVideoCallbackProxy, 2935140369);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlayRewardVideoCallbackProxy(Z_Construct_UClass_UPlayRewardVideoCallbackProxy, &UPlayRewardVideoCallbackProxy::StaticClass, TEXT("/Script/AdCollection"), TEXT("UPlayRewardVideoCallbackProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlayRewardVideoCallbackProxy);
	void UShowInterstitialCallbackProxy::StaticRegisterNativesUShowInterstitialCallbackProxy()
	{
		UClass* Class = UShowInterstitialCallbackProxy::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "ShowInterstitial", (Native)&UShowInterstitialCallbackProxy::execShowInterstitial },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 1);
	}
	UFunction* Z_Construct_UFunction_UShowInterstitialCallbackProxy_ShowInterstitial()
	{
		struct ShowInterstitialCallbackProxy_eventShowInterstitial_Parms
		{
			EAdType AdType;
			UShowInterstitialCallbackProxy* ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_UShowInterstitialCallbackProxy();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ShowInterstitial"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535, sizeof(ShowInterstitialCallbackProxy_eventShowInterstitial_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ReturnValue, ShowInterstitialCallbackProxy_eventShowInterstitial_Parms), 0x0010000000000580, Z_Construct_UClass_UShowInterstitialCallbackProxy_NoRegister());
			UProperty* NewProp_AdType = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("AdType"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(AdType, ShowInterstitialCallbackProxy_eventShowInterstitial_Parms), 0x0010000000000080, Z_Construct_UEnum_AdCollection_EAdType());
			UProperty* NewProp_AdType_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_AdType, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("BlueprintInternalUseOnly"), TEXT("true"));
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AdCollection"));
			MetaData->SetValue(ReturnFunction, TEXT("DisplayName"), TEXT("ShowInterstitial"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/ShowInterstitialCallbackProxy.h"));
			MetaData->SetValue(ReturnFunction, TEXT("WorldContext"), TEXT("WorldContextObject"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UShowInterstitialCallbackProxy_NoRegister()
	{
		return UShowInterstitialCallbackProxy::StaticClass();
	}
	UClass* Z_Construct_UClass_UShowInterstitialCallbackProxy()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UBlueprintAsyncActionBase();
			Z_Construct_UPackage__Script_AdCollection();
			OuterClass = UShowInterstitialCallbackProxy::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;

				OuterClass->LinkChild(Z_Construct_UFunction_UShowInterstitialCallbackProxy_ShowInterstitial());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_OnClose = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OnClose"), RF_Public|RF_Transient|RF_MarkAsNative) UMulticastDelegateProperty(CPP_PROPERTY_BASE(OnClose, UShowInterstitialCallbackProxy), 0x0010000010080000, Z_Construct_UDelegateFunction_AdCollection_DynamicInterstitialCloseDelegate__DelegateSignature());
				UProperty* NewProp_OnClick = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OnClick"), RF_Public|RF_Transient|RF_MarkAsNative) UMulticastDelegateProperty(CPP_PROPERTY_BASE(OnClick, UShowInterstitialCallbackProxy), 0x0010000010080000, Z_Construct_UDelegateFunction_AdCollection_DynamicInterstitialClickDelegate__DelegateSignature());
				UProperty* NewProp_OnShow = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OnShow"), RF_Public|RF_Transient|RF_MarkAsNative) UMulticastDelegateProperty(CPP_PROPERTY_BASE(OnShow, UShowInterstitialCallbackProxy), 0x0010000010080000, Z_Construct_UDelegateFunction_AdCollection_DynamicInterstitialShowDelegate__DelegateSignature());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UShowInterstitialCallbackProxy_ShowInterstitial(), "ShowInterstitial"); // 3361258101
				static TCppClassTypeInfo<TCppClassTypeTraits<UShowInterstitialCallbackProxy> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("ShowInterstitialCallbackProxy.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/ShowInterstitialCallbackProxy.h"));
				MetaData->SetValue(NewProp_OnClose, TEXT("ModuleRelativePath"), TEXT("Public/ShowInterstitialCallbackProxy.h"));
				MetaData->SetValue(NewProp_OnClick, TEXT("ModuleRelativePath"), TEXT("Public/ShowInterstitialCallbackProxy.h"));
				MetaData->SetValue(NewProp_OnShow, TEXT("ModuleRelativePath"), TEXT("Public/ShowInterstitialCallbackProxy.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UShowInterstitialCallbackProxy, 1833528409);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UShowInterstitialCallbackProxy(Z_Construct_UClass_UShowInterstitialCallbackProxy, &UShowInterstitialCallbackProxy::StaticClass, TEXT("/Script/AdCollection"), TEXT("UShowInterstitialCallbackProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UShowInterstitialCallbackProxy);
	UPackage* Z_Construct_UPackage__Script_AdCollection()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), nullptr, FName(TEXT("/Script/AdCollection")), false, false));
			ReturnPackage->SetPackageFlags(PKG_CompiledIn | 0x00000000);
			FGuid Guid;
			Guid.A = 0xAA6E9343;
			Guid.B = 0xFA99E6F0;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

			Z_Construct_UDelegateFunction_AdCollection_PlayRewardedDelegate__DelegateSignature();
			Z_Construct_UDelegateFunction_AdCollection_DynamicRewardedClosedDelegate__DelegateSignature();
			Z_Construct_UDelegateFunction_AdCollection_DynamicInterstitialShowDelegate__DelegateSignature();
			Z_Construct_UDelegateFunction_AdCollection_DynamicInterstitialClickDelegate__DelegateSignature();
			Z_Construct_UDelegateFunction_AdCollection_DynamicInterstitialCloseDelegate__DelegateSignature();
		}
		return ReturnPackage;
	}
#endif
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
