// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ADCOLLECTIONEDITOR_UnitySetting_generated_h
#error "UnitySetting.generated.h already included, missing '#pragma once' in UnitySetting.h"
#endif
#define ADCOLLECTIONEDITOR_UnitySetting_generated_h

#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_RPC_WRAPPERS
#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUnitySetting(); \
	friend ADCOLLECTIONEDITOR_API class UClass* Z_Construct_UClass_UUnitySetting(); \
public: \
	DECLARE_CLASS(UUnitySetting, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/AdCollectionEditor"), NO_API) \
	DECLARE_SERIALIZER(UUnitySetting) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUUnitySetting(); \
	friend ADCOLLECTIONEDITOR_API class UClass* Z_Construct_UClass_UUnitySetting(); \
public: \
	DECLARE_CLASS(UUnitySetting, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/AdCollectionEditor"), NO_API) \
	DECLARE_SERIALIZER(UUnitySetting) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUnitySetting(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUnitySetting) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUnitySetting); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUnitySetting); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUnitySetting(UUnitySetting&&); \
	NO_API UUnitySetting(const UUnitySetting&); \
public:


#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUnitySetting(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUnitySetting(UUnitySetting&&); \
	NO_API UUnitySetting(const UUnitySetting&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUnitySetting); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUnitySetting); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUnitySetting)


#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bIsAndroidEnabled() { return STRUCT_OFFSET(UUnitySetting, bIsAndroidEnabled); } \
	FORCEINLINE static uint32 __PPO__AndroidAppId() { return STRUCT_OFFSET(UUnitySetting, AndroidAppId); } \
	FORCEINLINE static uint32 __PPO__Placement() { return STRUCT_OFFSET(UUnitySetting, Placement); } \
	FORCEINLINE static uint32 __PPO__bIsIOSEnabled() { return STRUCT_OFFSET(UUnitySetting, bIsIOSEnabled); } \
	FORCEINLINE static uint32 __PPO__IOSAppId() { return STRUCT_OFFSET(UUnitySetting, IOSAppId); } \
	FORCEINLINE static uint32 __PPO__IOSPlacement() { return STRUCT_OFFSET(UUnitySetting, IOSPlacement); }


#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_16_PROLOG
#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_PRIVATE_PROPERTY_OFFSET \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_RPC_WRAPPERS \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_INCLASS \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_PRIVATE_PROPERTY_OFFSET \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_INCLASS_NO_PURE_DECLS \
	Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Bio2_Plugins_AdCollection_Source_AdCollectionEditor_Private_UnitySetting_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
