// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/AdCollectionEditor.h"
#include "AdCollectionEditor.generated.dep.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode1AdCollectionEditor() {}
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	COREUOBJECT_API class UClass* Z_Construct_UClass_UObject();

	ADCOLLECTIONEDITOR_API class UClass* Z_Construct_UClass_UAdMobSetting_NoRegister();
	ADCOLLECTIONEDITOR_API class UClass* Z_Construct_UClass_UAdMobSetting();
	ADCOLLECTIONEDITOR_API class UClass* Z_Construct_UClass_UChartBoostSetting_NoRegister();
	ADCOLLECTIONEDITOR_API class UClass* Z_Construct_UClass_UChartBoostSetting();
	ADCOLLECTIONEDITOR_API class UClass* Z_Construct_UClass_UFacebookSetting_NoRegister();
	ADCOLLECTIONEDITOR_API class UClass* Z_Construct_UClass_UFacebookSetting();
	ADCOLLECTIONEDITOR_API class UClass* Z_Construct_UClass_UUnitySetting_NoRegister();
	ADCOLLECTIONEDITOR_API class UClass* Z_Construct_UClass_UUnitySetting();
	ADCOLLECTIONEDITOR_API class UPackage* Z_Construct_UPackage__Script_AdCollectionEditor();
	void UAdMobSetting::StaticRegisterNativesUAdMobSetting()
	{
	}
	UClass* Z_Construct_UClass_UAdMobSetting_NoRegister()
	{
		return UAdMobSetting::StaticClass();
	}
	UClass* Z_Construct_UClass_UAdMobSetting()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UObject();
			Z_Construct_UPackage__Script_AdCollectionEditor();
			OuterClass = UAdMobSetting::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2000008C;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_IOSRewardedVideoAdUnit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IOSRewardedVideoAdUnit"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(IOSRewardedVideoAdUnit, UAdMobSetting), 0x0040000000004001);
				UProperty* NewProp_IOSInterstitialUnit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IOSInterstitialUnit"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(IOSInterstitialUnit, UAdMobSetting), 0x0040000000004001);
				UProperty* NewProp_IOSBannerUnit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IOSBannerUnit"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(IOSBannerUnit, UAdMobSetting), 0x0040000000004001);
				UProperty* NewProp_IOSAppId = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IOSAppId"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(IOSAppId, UAdMobSetting), 0x0040000000004001);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bIOSEnabled, UAdMobSetting, bool);
				UProperty* NewProp_bIOSEnabled = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bIOSEnabled"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bIOSEnabled, UAdMobSetting), 0x0040000000004001, CPP_BOOL_PROPERTY_BITMASK(bIOSEnabled, UAdMobSetting), sizeof(bool), true);
				UProperty* NewProp_AndroidRewardedVideoAdUnit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AndroidRewardedVideoAdUnit"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(AndroidRewardedVideoAdUnit, UAdMobSetting), 0x0040000000004001);
				UProperty* NewProp_AndroidInterstitialUnit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AndroidInterstitialUnit"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(AndroidInterstitialUnit, UAdMobSetting), 0x0040000000004001);
				UProperty* NewProp_AndroidBannerUnit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AndroidBannerUnit"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(AndroidBannerUnit, UAdMobSetting), 0x0040000000004001);
				UProperty* NewProp_AndroidAppId = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AndroidAppId"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(AndroidAppId, UAdMobSetting), 0x0040000000004001);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bAndroidEnabled, UAdMobSetting, bool);
				UProperty* NewProp_bAndroidEnabled = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bAndroidEnabled"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bAndroidEnabled, UAdMobSetting), 0x0040000000004001, CPP_BOOL_PROPERTY_BITMASK(bAndroidEnabled, UAdMobSetting), sizeof(bool), true);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->ClassConfigName = FName(TEXT("Engine"));
				static TCppClassTypeInfo<TCppClassTypeTraits<UAdMobSetting> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("AdMobSetting.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Private/AdMobSetting.h"));
				MetaData->SetValue(NewProp_IOSRewardedVideoAdUnit, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_IOSRewardedVideoAdUnit, TEXT("DisplayName"), TEXT("IOS RewardVideo AdUnit"));
				MetaData->SetValue(NewProp_IOSRewardedVideoAdUnit, TEXT("ModuleRelativePath"), TEXT("Private/AdMobSetting.h"));
				MetaData->SetValue(NewProp_IOSRewardedVideoAdUnit, TEXT("ToolTip"), TEXT("the admob ios rewardedvideo adunit"));
				MetaData->SetValue(NewProp_IOSInterstitialUnit, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_IOSInterstitialUnit, TEXT("DisplayName"), TEXT("IOS Interstitial AdUnit"));
				MetaData->SetValue(NewProp_IOSInterstitialUnit, TEXT("ModuleRelativePath"), TEXT("Private/AdMobSetting.h"));
				MetaData->SetValue(NewProp_IOSInterstitialUnit, TEXT("ToolTip"), TEXT("the admob ios interstitial adunit"));
				MetaData->SetValue(NewProp_IOSBannerUnit, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_IOSBannerUnit, TEXT("DisplayName"), TEXT("IOS Banner AdUnit"));
				MetaData->SetValue(NewProp_IOSBannerUnit, TEXT("ModuleRelativePath"), TEXT("Private/AdMobSetting.h"));
				MetaData->SetValue(NewProp_IOSBannerUnit, TEXT("ToolTip"), TEXT("the admob ios banner adunit"));
				MetaData->SetValue(NewProp_IOSAppId, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_IOSAppId, TEXT("DisplayName"), TEXT("IOS AppID"));
				MetaData->SetValue(NewProp_IOSAppId, TEXT("ModuleRelativePath"), TEXT("Private/AdMobSetting.h"));
				MetaData->SetValue(NewProp_IOSAppId, TEXT("ToolTip"), TEXT("the admob ios app id"));
				MetaData->SetValue(NewProp_bIOSEnabled, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_bIOSEnabled, TEXT("DisplayName"), TEXT("If AdMob IOS Enable"));
				MetaData->SetValue(NewProp_bIOSEnabled, TEXT("ModuleRelativePath"), TEXT("Private/AdMobSetting.h"));
				MetaData->SetValue(NewProp_bIOSEnabled, TEXT("ToolTip"), TEXT("if the ios admob enable"));
				MetaData->SetValue(NewProp_AndroidRewardedVideoAdUnit, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_AndroidRewardedVideoAdUnit, TEXT("DisplayName"), TEXT("Android RewardVideo AdUnit"));
				MetaData->SetValue(NewProp_AndroidRewardedVideoAdUnit, TEXT("ModuleRelativePath"), TEXT("Private/AdMobSetting.h"));
				MetaData->SetValue(NewProp_AndroidRewardedVideoAdUnit, TEXT("ToolTip"), TEXT("the rewardedvideo adunit"));
				MetaData->SetValue(NewProp_AndroidInterstitialUnit, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_AndroidInterstitialUnit, TEXT("DisplayName"), TEXT("Android Interstitial AdUnit"));
				MetaData->SetValue(NewProp_AndroidInterstitialUnit, TEXT("ModuleRelativePath"), TEXT("Private/AdMobSetting.h"));
				MetaData->SetValue(NewProp_AndroidInterstitialUnit, TEXT("ToolTip"), TEXT("the interstitial adunit"));
				MetaData->SetValue(NewProp_AndroidBannerUnit, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_AndroidBannerUnit, TEXT("DisplayName"), TEXT("Android Banner AdUnit"));
				MetaData->SetValue(NewProp_AndroidBannerUnit, TEXT("ModuleRelativePath"), TEXT("Private/AdMobSetting.h"));
				MetaData->SetValue(NewProp_AndroidBannerUnit, TEXT("ToolTip"), TEXT("the banner adunit"));
				MetaData->SetValue(NewProp_AndroidAppId, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_AndroidAppId, TEXT("DisplayName"), TEXT("Android AppID"));
				MetaData->SetValue(NewProp_AndroidAppId, TEXT("ModuleRelativePath"), TEXT("Private/AdMobSetting.h"));
				MetaData->SetValue(NewProp_AndroidAppId, TEXT("ToolTip"), TEXT("the android admob appid"));
				MetaData->SetValue(NewProp_bAndroidEnabled, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_bAndroidEnabled, TEXT("DisplayName"), TEXT("If AdMob Android Enable"));
				MetaData->SetValue(NewProp_bAndroidEnabled, TEXT("ModuleRelativePath"), TEXT("Private/AdMobSetting.h"));
				MetaData->SetValue(NewProp_bAndroidEnabled, TEXT("ToolTip"), TEXT("if the android admob enable"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAdMobSetting, 343972062);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAdMobSetting(Z_Construct_UClass_UAdMobSetting, &UAdMobSetting::StaticClass, TEXT("/Script/AdCollectionEditor"), TEXT("UAdMobSetting"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAdMobSetting);
	void UChartBoostSetting::StaticRegisterNativesUChartBoostSetting()
	{
	}
	UClass* Z_Construct_UClass_UChartBoostSetting_NoRegister()
	{
		return UChartBoostSetting::StaticClass();
	}
	UClass* Z_Construct_UClass_UChartBoostSetting()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UObject();
			Z_Construct_UPackage__Script_AdCollectionEditor();
			OuterClass = UChartBoostSetting::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2000008C;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_IOSSignature = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IOSSignature"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(IOSSignature, UChartBoostSetting), 0x0040000000004001);
				UProperty* NewProp_IOSAppId = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IOSAppId"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(IOSAppId, UChartBoostSetting), 0x0040000000004001);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bIsIOSEnabled, UChartBoostSetting, bool);
				UProperty* NewProp_bIsIOSEnabled = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bIsIOSEnabled"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bIsIOSEnabled, UChartBoostSetting), 0x0040000000004001, CPP_BOOL_PROPERTY_BITMASK(bIsIOSEnabled, UChartBoostSetting), sizeof(bool), true);
				UProperty* NewProp_AndroidSignature = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AndroidSignature"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(AndroidSignature, UChartBoostSetting), 0x0040000000004001);
				UProperty* NewProp_AndroidAppId = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AndroidAppId"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(AndroidAppId, UChartBoostSetting), 0x0040000000004001);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bIsAndroidEnabled, UChartBoostSetting, bool);
				UProperty* NewProp_bIsAndroidEnabled = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bIsAndroidEnabled"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bIsAndroidEnabled, UChartBoostSetting), 0x0040000000004001, CPP_BOOL_PROPERTY_BITMASK(bIsAndroidEnabled, UChartBoostSetting), sizeof(bool), true);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->ClassConfigName = FName(TEXT("Engine"));
				static TCppClassTypeInfo<TCppClassTypeTraits<UChartBoostSetting> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("ChartBoostSetting.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Private/ChartBoostSetting.h"));
				MetaData->SetValue(NewProp_IOSSignature, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_IOSSignature, TEXT("DisplayName"), TEXT("IOS Signature"));
				MetaData->SetValue(NewProp_IOSSignature, TEXT("ModuleRelativePath"), TEXT("Private/ChartBoostSetting.h"));
				MetaData->SetValue(NewProp_IOSSignature, TEXT("ToolTip"), TEXT("the ios chartboost signature"));
				MetaData->SetValue(NewProp_IOSAppId, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_IOSAppId, TEXT("DisplayName"), TEXT("IOS AppID"));
				MetaData->SetValue(NewProp_IOSAppId, TEXT("ModuleRelativePath"), TEXT("Private/ChartBoostSetting.h"));
				MetaData->SetValue(NewProp_IOSAppId, TEXT("ToolTip"), TEXT("the ios chartboost appid"));
				MetaData->SetValue(NewProp_bIsIOSEnabled, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_bIsIOSEnabled, TEXT("DisplayName"), TEXT("If IOS Enable"));
				MetaData->SetValue(NewProp_bIsIOSEnabled, TEXT("ModuleRelativePath"), TEXT("Private/ChartBoostSetting.h"));
				MetaData->SetValue(NewProp_bIsIOSEnabled, TEXT("ToolTip"), TEXT("if the ios chartboost enable"));
				MetaData->SetValue(NewProp_AndroidSignature, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_AndroidSignature, TEXT("DisplayName"), TEXT("Android Signature"));
				MetaData->SetValue(NewProp_AndroidSignature, TEXT("ModuleRelativePath"), TEXT("Private/ChartBoostSetting.h"));
				MetaData->SetValue(NewProp_AndroidSignature, TEXT("ToolTip"), TEXT("the android app signature"));
				MetaData->SetValue(NewProp_AndroidAppId, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_AndroidAppId, TEXT("DisplayName"), TEXT("Android AppID"));
				MetaData->SetValue(NewProp_AndroidAppId, TEXT("ModuleRelativePath"), TEXT("Private/ChartBoostSetting.h"));
				MetaData->SetValue(NewProp_AndroidAppId, TEXT("ToolTip"), TEXT("the android chartboost appid"));
				MetaData->SetValue(NewProp_bIsAndroidEnabled, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_bIsAndroidEnabled, TEXT("DisplayName"), TEXT("If Android Enable"));
				MetaData->SetValue(NewProp_bIsAndroidEnabled, TEXT("ModuleRelativePath"), TEXT("Private/ChartBoostSetting.h"));
				MetaData->SetValue(NewProp_bIsAndroidEnabled, TEXT("ToolTip"), TEXT("if the android chartboost enable"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UChartBoostSetting, 4095735445);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UChartBoostSetting(Z_Construct_UClass_UChartBoostSetting, &UChartBoostSetting::StaticClass, TEXT("/Script/AdCollectionEditor"), TEXT("UChartBoostSetting"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UChartBoostSetting);
	void UFacebookSetting::StaticRegisterNativesUFacebookSetting()
	{
	}
	UClass* Z_Construct_UClass_UFacebookSetting_NoRegister()
	{
		return UFacebookSetting::StaticClass();
	}
	UClass* Z_Construct_UClass_UFacebookSetting()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UObject();
			Z_Construct_UPackage__Script_AdCollectionEditor();
			OuterClass = UFacebookSetting::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2000008C;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_IOSRewardedVideoAdUnit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IOSRewardedVideoAdUnit"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(IOSRewardedVideoAdUnit, UFacebookSetting), 0x0040000000004001);
				UProperty* NewProp_IOSInterstitialUnit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IOSInterstitialUnit"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(IOSInterstitialUnit, UFacebookSetting), 0x0040000000004001);
				UProperty* NewProp_IOSBannerUnit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IOSBannerUnit"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(IOSBannerUnit, UFacebookSetting), 0x0040000000004001);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bIOSEnabled, UFacebookSetting, bool);
				UProperty* NewProp_bIOSEnabled = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bIOSEnabled"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bIOSEnabled, UFacebookSetting), 0x0040000000004001, CPP_BOOL_PROPERTY_BITMASK(bIOSEnabled, UFacebookSetting), sizeof(bool), true);
				UProperty* NewProp_AndroidRewardedVideoAdUnit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AndroidRewardedVideoAdUnit"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(AndroidRewardedVideoAdUnit, UFacebookSetting), 0x0040000000004001);
				UProperty* NewProp_AndroidInterstitialUnit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AndroidInterstitialUnit"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(AndroidInterstitialUnit, UFacebookSetting), 0x0040000000004001);
				UProperty* NewProp_AndroidBannerUnit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AndroidBannerUnit"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(AndroidBannerUnit, UFacebookSetting), 0x0040000000004001);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bAndroidEnabled, UFacebookSetting, bool);
				UProperty* NewProp_bAndroidEnabled = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bAndroidEnabled"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bAndroidEnabled, UFacebookSetting), 0x0040000000004001, CPP_BOOL_PROPERTY_BITMASK(bAndroidEnabled, UFacebookSetting), sizeof(bool), true);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->ClassConfigName = FName(TEXT("Engine"));
				static TCppClassTypeInfo<TCppClassTypeTraits<UFacebookSetting> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("FacebookSetting.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Private/FacebookSetting.h"));
				MetaData->SetValue(NewProp_IOSRewardedVideoAdUnit, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_IOSRewardedVideoAdUnit, TEXT("DisplayName"), TEXT("IOS RewardVideo Placement"));
				MetaData->SetValue(NewProp_IOSRewardedVideoAdUnit, TEXT("ModuleRelativePath"), TEXT("Private/FacebookSetting.h"));
				MetaData->SetValue(NewProp_IOSInterstitialUnit, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_IOSInterstitialUnit, TEXT("DisplayName"), TEXT("IOS Interstitial Placement"));
				MetaData->SetValue(NewProp_IOSInterstitialUnit, TEXT("ModuleRelativePath"), TEXT("Private/FacebookSetting.h"));
				MetaData->SetValue(NewProp_IOSBannerUnit, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_IOSBannerUnit, TEXT("DisplayName"), TEXT("IOS Banner Placement"));
				MetaData->SetValue(NewProp_IOSBannerUnit, TEXT("ModuleRelativePath"), TEXT("Private/FacebookSetting.h"));
				MetaData->SetValue(NewProp_bIOSEnabled, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_bIOSEnabled, TEXT("DisplayName"), TEXT("If Facebook IOS Enable"));
				MetaData->SetValue(NewProp_bIOSEnabled, TEXT("ModuleRelativePath"), TEXT("Private/FacebookSetting.h"));
				MetaData->SetValue(NewProp_AndroidRewardedVideoAdUnit, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_AndroidRewardedVideoAdUnit, TEXT("DisplayName"), TEXT("Android RewardVideo Placement"));
				MetaData->SetValue(NewProp_AndroidRewardedVideoAdUnit, TEXT("ModuleRelativePath"), TEXT("Private/FacebookSetting.h"));
				MetaData->SetValue(NewProp_AndroidRewardedVideoAdUnit, TEXT("ToolTip"), TEXT("the rewardedvideo adunit"));
				MetaData->SetValue(NewProp_AndroidInterstitialUnit, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_AndroidInterstitialUnit, TEXT("DisplayName"), TEXT("Android Interstitial Placement"));
				MetaData->SetValue(NewProp_AndroidInterstitialUnit, TEXT("ModuleRelativePath"), TEXT("Private/FacebookSetting.h"));
				MetaData->SetValue(NewProp_AndroidInterstitialUnit, TEXT("ToolTip"), TEXT("the interstitial adunit"));
				MetaData->SetValue(NewProp_AndroidBannerUnit, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_AndroidBannerUnit, TEXT("DisplayName"), TEXT("Android Banner Placement"));
				MetaData->SetValue(NewProp_AndroidBannerUnit, TEXT("ModuleRelativePath"), TEXT("Private/FacebookSetting.h"));
				MetaData->SetValue(NewProp_AndroidBannerUnit, TEXT("ToolTip"), TEXT("the banner adunit"));
				MetaData->SetValue(NewProp_bAndroidEnabled, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_bAndroidEnabled, TEXT("DisplayName"), TEXT("If Facebook Android Enable"));
				MetaData->SetValue(NewProp_bAndroidEnabled, TEXT("ModuleRelativePath"), TEXT("Private/FacebookSetting.h"));
				MetaData->SetValue(NewProp_bAndroidEnabled, TEXT("ToolTip"), TEXT("if the android facebook enable"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFacebookSetting, 3169566319);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFacebookSetting(Z_Construct_UClass_UFacebookSetting, &UFacebookSetting::StaticClass, TEXT("/Script/AdCollectionEditor"), TEXT("UFacebookSetting"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFacebookSetting);
	void UUnitySetting::StaticRegisterNativesUUnitySetting()
	{
	}
	UClass* Z_Construct_UClass_UUnitySetting_NoRegister()
	{
		return UUnitySetting::StaticClass();
	}
	UClass* Z_Construct_UClass_UUnitySetting()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UObject();
			Z_Construct_UPackage__Script_AdCollectionEditor();
			OuterClass = UUnitySetting::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2000008C;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_IOSPlacement = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IOSPlacement"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(IOSPlacement, UUnitySetting), 0x0040000000004001);
				UProperty* NewProp_IOSAppId = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IOSAppId"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(IOSAppId, UUnitySetting), 0x0040000000004001);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bIsIOSEnabled, UUnitySetting, bool);
				UProperty* NewProp_bIsIOSEnabled = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bIsIOSEnabled"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bIsIOSEnabled, UUnitySetting), 0x0040000000004001, CPP_BOOL_PROPERTY_BITMASK(bIsIOSEnabled, UUnitySetting), sizeof(bool), true);
				UProperty* NewProp_Placement = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Placement"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(Placement, UUnitySetting), 0x0040000000004001);
				UProperty* NewProp_AndroidAppId = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AndroidAppId"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(AndroidAppId, UUnitySetting), 0x0040000000004001);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bIsAndroidEnabled, UUnitySetting, bool);
				UProperty* NewProp_bIsAndroidEnabled = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bIsAndroidEnabled"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bIsAndroidEnabled, UUnitySetting), 0x0040000000004001, CPP_BOOL_PROPERTY_BITMASK(bIsAndroidEnabled, UUnitySetting), sizeof(bool), true);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->ClassConfigName = FName(TEXT("Engine"));
				static TCppClassTypeInfo<TCppClassTypeTraits<UUnitySetting> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("UnitySetting.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Private/UnitySetting.h"));
				MetaData->SetValue(NewProp_IOSPlacement, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_IOSPlacement, TEXT("DisplayName"), TEXT("Unity IOS Placement"));
				MetaData->SetValue(NewProp_IOSPlacement, TEXT("ModuleRelativePath"), TEXT("Private/UnitySetting.h"));
				MetaData->SetValue(NewProp_IOSAppId, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_IOSAppId, TEXT("DisplayName"), TEXT("Unity IOS AppID"));
				MetaData->SetValue(NewProp_IOSAppId, TEXT("ModuleRelativePath"), TEXT("Private/UnitySetting.h"));
				MetaData->SetValue(NewProp_IOSAppId, TEXT("ToolTip"), TEXT("the ios unity appid"));
				MetaData->SetValue(NewProp_bIsIOSEnabled, TEXT("Category"), TEXT("IOS"));
				MetaData->SetValue(NewProp_bIsIOSEnabled, TEXT("DisplayName"), TEXT("If Is Unity IOS Enable"));
				MetaData->SetValue(NewProp_bIsIOSEnabled, TEXT("ModuleRelativePath"), TEXT("Private/UnitySetting.h"));
				MetaData->SetValue(NewProp_bIsIOSEnabled, TEXT("ToolTip"), TEXT("if the ios unity ads enable"));
				MetaData->SetValue(NewProp_Placement, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_Placement, TEXT("DisplayName"), TEXT("Unity Android Placement"));
				MetaData->SetValue(NewProp_Placement, TEXT("ModuleRelativePath"), TEXT("Private/UnitySetting.h"));
				MetaData->SetValue(NewProp_AndroidAppId, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_AndroidAppId, TEXT("DisplayName"), TEXT("Unity Android AppID"));
				MetaData->SetValue(NewProp_AndroidAppId, TEXT("ModuleRelativePath"), TEXT("Private/UnitySetting.h"));
				MetaData->SetValue(NewProp_AndroidAppId, TEXT("ToolTip"), TEXT("the android unity appid"));
				MetaData->SetValue(NewProp_bIsAndroidEnabled, TEXT("Category"), TEXT("Android"));
				MetaData->SetValue(NewProp_bIsAndroidEnabled, TEXT("DisplayName"), TEXT("If Is Unity Android Enable"));
				MetaData->SetValue(NewProp_bIsAndroidEnabled, TEXT("ModuleRelativePath"), TEXT("Private/UnitySetting.h"));
				MetaData->SetValue(NewProp_bIsAndroidEnabled, TEXT("ToolTip"), TEXT("if the android unity ads enable"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUnitySetting, 2263962444);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUnitySetting(Z_Construct_UClass_UUnitySetting, &UUnitySetting::StaticClass, TEXT("/Script/AdCollectionEditor"), TEXT("UUnitySetting"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUnitySetting);
	UPackage* Z_Construct_UPackage__Script_AdCollectionEditor()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), nullptr, FName(TEXT("/Script/AdCollectionEditor")), false, false));
			ReturnPackage->SetPackageFlags(PKG_CompiledIn | 0x00000040);
			FGuid Guid;
			Guid.A = 0x910A8A62;
			Guid.B = 0x561FAE0D;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
